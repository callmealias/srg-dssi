﻿using System;
using System.Collections.Generic;
using System.Xml;
using Microsoft.VisualBasic.FileIO;

namespace SrgContactUpdater
{
    class ConfigSettings
    {
        public bool DebugMode { get; set; } = true;
        public bool TestMode { get; set; } = false;
        public string Username { get; set; }
        public string Password { get; set; }
        public string AppPath { get; set; }
        public string ExclusionFile { get; set; }
        public string ContactsFile { get; set; }
        public string OverrideFile { get; set; }
        public string JobsFile { get; set; }
        public string LocationFile { get; set; }


        public ConfigSettings(string filename)
        {

            try
            {
                // parse config file
                XmlDocument xmlDoc = new XmlDocument(); // Create an XML document object
                xmlDoc.Load(filename); // Load the XML document from the specified file

                // Get elements
                XmlNodeList username = xmlDoc.GetElementsByTagName("Username");
                XmlNodeList password = xmlDoc.GetElementsByTagName("Password");
                AppPath = xmlDoc.GetElementsByTagName("ConfigPath")[0].InnerText;
                XmlNodeList contacts = xmlDoc.GetElementsByTagName("ContactsFile");
                XmlNodeList debug = xmlDoc.GetElementsByTagName("DebugMode");
                XmlNodeList test = xmlDoc.GetElementsByTagName("TestMode");

                // save config data
                this.Username = username[0].InnerText;
                this.Password = password[0].InnerText;

                var dateContact = DateTime.Now.ToString("MM-dd-yyyy");
                this.ContactsFile = @""+ AppPath + "\\" + contacts[0]["BaseName"].InnerText + dateContact + "." + contacts[0]["Extension"].InnerText;

                this.JobsFile = AppPath + "\\jobs.csv";
                this.LocationFile = AppPath + "\\locations.csv";
                this.OverrideFile = AppPath + "\\overrides.csv";
                this.ExclusionFile = AppPath + "\\exclusions.csv";

                this.DebugMode = Convert.ToBoolean(debug[0].InnerText);
                this.TestMode = Convert.ToBoolean(test[0].InnerText);
            }
            catch
            {
                // intentionally left blank, ignore xml parse errors
            }
        }

        public List<Override> GetOverrides()
        {
            // parse overrides file
            List<Override> overrides = new List<Override>();
            Override newOverride;
            TextFieldParser parser = new TextFieldParser(this.OverrideFile);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            while (!parser.EndOfData)
            {
                //Process row
                string[] fields = parser.ReadFields();

                newOverride = new Override();
                newOverride.Name = fields[0];
                newOverride.EmployeeId = fields[1];
                newOverride.ProfileId = Int32.Parse(fields[2]);
                newOverride.Location1Id = fields[3];

                if(fields.Length > 4)
                    newOverride.Location2Id = fields[4];
                if(fields.Length > 5)
                    newOverride.Location3Id = fields[5];
                if(fields.Length > 6)
                    newOverride.Location4Id = fields[6];
                if(fields.Length > 7)
                    newOverride.Location5Id = fields[7];
                if (fields.Length > 8)
                    newOverride.Location6Id = fields[8];
                if (fields.Length > 9)
                    newOverride.Location7Id = fields[9];
                if (fields.Length > 10)
                    newOverride.Location8Id = fields[10];
                if (fields.Length > 11)
                    newOverride.Location9Id = fields[11];

                overrides.Add(newOverride);
            }

            return overrides;
        }

        public List<Exclusion> GetExclusions()
        {
            // parse exclusions file
            List<Exclusion> exclusions = new List<Exclusion>();
            Exclusion newExclusion;
            TextFieldParser parser = new TextFieldParser(this.ExclusionFile);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            while (!parser.EndOfData)
            {
                //Process row
                string[] fields = parser.ReadFields();

                newExclusion = new Exclusion();
                newExclusion.Name = fields[0];
                newExclusion.MasterUserId = Int32.Parse(fields[1]);

                exclusions.Add(newExclusion);
            }

            return exclusions;
        }

        public List<Contact> GetContactsCSV(List<JobCodes> jobs, List<Location> locations, List<Override> overrides)
        {
            // parse exclusions file
            List<Contact> contacts = new List<Contact>();
            Contact newContact;
            TextFieldParser parser = new TextFieldParser(this.ContactsFile);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            int num = 0;
            while (!parser.EndOfData)
            {
                //Process row
                string[] fields = parser.ReadFields();

                // ignore 1st row and header information && empty rows
                if (num++ == 0 || fields[0] == "")
                {
                    continue;
                }

                newContact = new Contact();
                newContact.FirstName = fields[0].Length > 15 ? fields[0].Substring(0, 15) : fields[0];
                newContact.LastName = fields[1].Length > 15 ? fields[1].Substring(0, 15) : fields[1];
                //samaccountname?
                newContact.UserName = fields[2];
                newContact.EmailAddress = fields[3];
                newContact.EmployeeId = fields[4];//Convert.ToInt32(fields[3]); for Int
                //Console.WriteLine("EMPLOYEEID: "+newContact.EmployeeId+" :: "+fields[4]);

                newContact.JobCode = fields[6];
                newContact.ProfileId = null; //Default to lowest level
                newContact.CheckFacilityAccess = false;

                Override overrideContact = overrides.Find(x => x.EmployeeId == newContact.EmployeeId);
                if (overrideContact != null)
                {
                    //Console.WriteLine(newContact.FirstName + " " + newContact.LastName + " " + overrideContact.Location1Id);

                    newContact.ProfileId = overrideContact.ProfileId;
            
                    newContact.Location1Code = overrideContact.Location1Id;

                    if (overrideContact.Location2Id != null && overrideContact.Location2Id.Trim() != "")
                    {

                        newContact.CheckFacilityAccess = true;
                        newContact.Facilities = new DssiContact.FacilityList();

                        Int32 FacilitiesSize = 2;
                        if (overrideContact.Location9Id != null && overrideContact.Location9Id.Trim() != "")
                        {
                            FacilitiesSize = 9;
                        }
                        else if (overrideContact.Location8Id != null && overrideContact.Location8Id.Trim() != "")
                        {
                            FacilitiesSize = 8;
                        }
                        else if (overrideContact.Location7Id != null && overrideContact.Location7Id.Trim() != "")
                        {
                            FacilitiesSize = 7;
                        }
                        else if (overrideContact.Location6Id != null && overrideContact.Location6Id.Trim() != "")
                        {
                            FacilitiesSize = 6;
                        }
                        else if (overrideContact.Location5Id != null && overrideContact.Location5Id.Trim() != "")
                        {
                           FacilitiesSize = 5;
                        }
                        else if (overrideContact.Location4Id != null && overrideContact.Location4Id.Trim() != "")
                        {
                            FacilitiesSize = 4;
                        }
                        else if (overrideContact.Location3Id != null && overrideContact.Location3Id.Trim() != "")
                        {
                            FacilitiesSize = 3;
                        }
                        //Console.WriteLine(FacilitiesSize);
                        DssiContact.Facility[] facilities = new DssiContact.Facility[FacilitiesSize];// addFacilities = null;

                        facilities[0] = new DssiContact.Facility { FacilityID = overrideContact.Location1Id };
                        facilities[1] = new DssiContact.Facility { FacilityID = overrideContact.Location2Id };

                        if (overrideContact.Location3Id != null && overrideContact.Location3Id.Trim() != "")
                        {
                            facilities[2] = new DssiContact.Facility { FacilityID = overrideContact.Location3Id };
                        }
                        if (overrideContact.Location4Id != null && overrideContact.Location4Id.Trim() != "")
                        {
                            facilities[3] = new DssiContact.Facility { FacilityID = overrideContact.Location4Id };
                        }
                        if (overrideContact.Location5Id != null && overrideContact.Location5Id.Trim() != "")
                        {
                            facilities[4] = new DssiContact.Facility { FacilityID = overrideContact.Location5Id };
                        }
                        if (overrideContact.Location6Id != null && overrideContact.Location6Id.Trim() != "")
                        {
                            facilities[5] = new DssiContact.Facility { FacilityID = overrideContact.Location6Id };
                        }
                        if (overrideContact.Location7Id != null && overrideContact.Location7Id.Trim() != "")
                        {
                            facilities[6] = new DssiContact.Facility { FacilityID = overrideContact.Location7Id };
                        }
                        if (overrideContact.Location8Id != null && overrideContact.Location8Id.Trim() != "")
                        {
                            facilities[7] = new DssiContact.Facility { FacilityID = overrideContact.Location8Id };
                        }
                        if (overrideContact.Location9Id != null && overrideContact.Location9Id.Trim() != "")
                        {
                            facilities[8] = new DssiContact.Facility { FacilityID = overrideContact.Location9Id };
                        }

                        newContact.Facilities.Facilities = facilities;
                    }
                }
                else
                {

                    foreach (var job in jobs)
                    {
                        if (newContact.EmployeeId == "124954")
                        {
                            //Console.WriteLine(job.JobCode + " == " + newContact.JobCode);
                        }
                        if (job.JobCode == newContact.JobCode)
                        {
                            newContact.ProfileId = job.ProfileId;
                        }
                    }
                    if (newContact.ProfileId == null)
                    {
                        //Console.WriteLine("EMPLOYEEID: " + newContact.EmployeeId + " :: " + newContact.JobCode);
                        continue;
                    }

                    newContact.LocationName = fields[5];
                    foreach (var location in locations)
                    {
                        if (location.LocationName == newContact.LocationName)
                        {
                            //Console.WriteLine(newContact.FirstName+ " " + newContact.LastName+ " " +location.LocationName + " " + location.LocationCode);
                            if (location.Ignore)
                            {
                                //Bypass contacts from locations that have an ignore flag
                                continue;
                            }
                            newContact.Location1Code = location.LocationCode;
                        }

                    }
                }
                //newContact.Enabled = fields[7];//TODO

                contacts.Add(newContact);
               
            }

            return contacts;
        }

        public List<JobCodes> getJobs()
        {
            List<JobCodes> jobs = new List<JobCodes>();
            JobCodes newJob;
            TextFieldParser parser = new TextFieldParser(this.JobsFile);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            int num = 0;
            while (!parser.EndOfData)
            {
                //Process row
                string[] fields = parser.ReadFields();

                // ignore 1st row and header information
                if (num++ == 0)
                {
                    //continue;
                }

                newJob = new JobCodes();
                newJob.JobCode = fields[0];
                newJob.ProfileName = fields[1];
                newJob.ProfileId = Int32.Parse(fields[2]);
                

                jobs.Add(newJob);
            }

            return jobs;
        }

        public List<Location> getLocations()
        {
            List<Location> locations = new List<Location>();
            Location newLocation;
            TextFieldParser parser = new TextFieldParser(this.LocationFile);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            int num = 0;
            while (!parser.EndOfData)
            {
                //Process row
                string[] fields = parser.ReadFields();

                // ignore 1st row and header information
                if (num++ == 0)
                {
                    continue;
                }

                newLocation = new Location();
                newLocation.LocationCode = fields[0];
                newLocation.LocationName = fields[1];
                newLocation.Ignore = false;
                if (fields.Length > 2)
                {
                    newLocation.Ignore = true;
                }

                locations.Add(newLocation);
            }

            return locations;
        }

      

    }
}
