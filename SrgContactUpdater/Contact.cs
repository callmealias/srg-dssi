﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrgContactUpdater
{

    public class Contact
    {
        public String LocationName { get; set; }
        public String Location1Code { get; set; }
        public String Location2Code { get; set; }
        public String Location3Code { get; set; }
        public String Location4Code { get; set; }
        public String Location5Code { get; set; }
        public String Location6Code { get; set; }
        public String Location7Code { get; set; }
        public String Location8Code { get; set; }
        public String Location9Code { get; set; }

        public Boolean CheckFacilityAccess { get; set; }
        public DssiContact.FacilityList Facilities { get; set; }
        public DssiContact.UserFlags UserFlags { get; set; }

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String UserName { get; set; }
        public String UserStatus { get; set; }
        public String UserTestFlag { get; set; }
        public String JobCode { get; set; }
        public String UserProfile { get; set; }
        public Nullable<Int32> ProfileId { get; set; }
        public String EmailAddress { get; set; }
        public String EmployeeId { get; set; }
        public String RsiAccess { get; set; }
        public String Enabled { get; set;}
    
    


    }
}
