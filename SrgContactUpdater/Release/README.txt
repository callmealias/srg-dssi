Startup
=======

You must pass in the filename of the XML config file. If there's a space in the directory path, you must enclose it in quotes. Example:
"-config:C:\Users\Stephane Deuvaert\source\repos\srg-dssi\SrgContactUpdater\Release\SRGContactUpdaterSettings.xml"

All other configuration files must reside in the same directory.


Logging
=======

All output is logged to log.txt in the config directory.


Exclusions
==========

A few accounts on DSSI should be ignored by the app. These accounts are listed in exclusions.csv in this format without a header row:

Name (for reference) 	| 	MasterUserId
Auto Approve			|	173584

Accounts with an email address ending in dssi.net, directsupply.com or directs.com will be ignored by default. Any contacts in this file must be updated manually.


Overrides
=========

Accounts attached to more than one facility or with permissions that differ from job title will need to be added to the overrides.csv file in this format without a header row:

Name (for reference)	|	EmployeeId	|	ProfileId	| Location 1 Id	|	Location 2 Id	| Location 3 Id	| Location 4 Id	| Location 5 Id | Location 6 Id | Location 7 Id | Location 8 Id | Location 9 Id
Bill Diamond			|	112682		|	2911		|	145			|	240


Locations
=========

The locations.csv file handles translation between DSSI location Ids and SRG location names. Users with access to more than one location must be entered in the overrides file.


DSSI Profiles
=============

Access on DSSI is determined by profiles. Profile Ids are as follows:

profile name				|	profile id
Super User					|	2913
Super User - No Ordering	|	4557
Facility Administrator		|	2911
Business Office Manager		|	2944
Order Entry					|	2910

Profiles are determined by job title. The translation file is jobs.csv and translates DSSI profiles to SRG job titles.


Active Directory Export
=======================

This app expects to find a csv file with the following naming convention: AD_Users_MM.DD.YY.csv in the config directory with the following columns:

First Name	|	Last Name	|	Full Name			|	Email Address					|	Employee Id	| 	Facility Name				|	Job Title
Denise		|	Aaron-Hoyte	|	Denise.Aaron-Hoyte	|	Denise.Aaron-Hoyte@SRG-LLC.com	|	120207		|	The Piedmont at Buckhead	|	Housekeeping Director

Users found on DSSI but not in this file will be deactivated unless they are excluded (see Exclusions above).


Configuration File
==================

API username and password as well as name and format of the override, contact, jobs, locations, and exclusion files are listed in XML format.

