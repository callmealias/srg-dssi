﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SrgContactUpdater
{
    class Program
    {
        public static string log;
        public static bool debug; // output contact info to log file
        public static bool test; // when set to TRUE no changes sent to API endpoint
        public static ConfigSettings config;
        public static Int32 FacilityAdmin = 2911;

        static void Main(string[] args)
        {
            Program.WriteLine("Starting SRG Contact Updater");

            // parse arguements
            var confFile = args.SingleOrDefault(arg => arg.StartsWith("-config:"));
            if (!string.IsNullOrEmpty(confFile))
            {
                confFile = confFile.Replace("-config:", "");
            }

            // load settings from config file
            Program.config = new ConfigSettings(confFile);
            Program.debug = config.DebugMode;
            Program.test = config.TestMode;

            Program.WriteLine("Loaded Configuration File: " + confFile);
            Program.WriteLine("Debug value: " + Program.debug);
            Program.WriteLine("Test value: " + Program.test);

            List<JobCodes> jobs = config.getJobs();// load jobs list
            List<Location> locations = config.getLocations();// load locations list            
            List<Override> overrides = config.GetOverrides();// load overrides list
            List<Exclusion> exclusions = config.GetExclusions();// load exclusion list
            List<Contact> contacts = config.GetContactsCSV(jobs, locations, overrides);// load contacts list

            Program.WriteLine("Imported " + overrides.Count + " overrides from Overrides File: " + config.OverrideFile);
            Program.WriteLine("Imported " + exclusions.Count + " exclusions from Exclusions File: " + config.ExclusionFile);
            Program.WriteLine("Imported " + contacts.Count + " contacts from Contacts File: " + config.ContactsFile);

            // setup DSSI endpoint + get all active contacts
            DssiContact.ContactService2Client contactService = new DssiContact.ContactService2Client("IContactService2");
            contactService.ClientCredentials.UserName.UserName = config.Username;
            contactService.ClientCredentials.UserName.Password = config.Password;
            DssiContact.ContactList2 liveContacts = new DssiContact.ContactList2();
            DssiContact.ApprovalGroupList approvalGroups = new DssiContact.ApprovalGroupList();


            try
            {
                liveContacts = contactService.GetAllActiveContacts();
                Program.WriteLine("Retrieved " + liveContacts.Contacts.Length + " from DSSI");

                approvalGroups = contactService.GetAllApprovalGroups();
            }
            catch (Exception e)
            {
                Program.WriteLine("" + e);
                Program.WriteLog();
                return;
            }

            // check each contact
            var numContactsUpdated = 0;
            var numContactsCreated = 0;
            foreach (var contact in contacts)
            {
                // check overrides list: employees listed in the overrides file have roles and permissions based on the file, not based on AD
                var createRequired = false;
                var mismatch = "";
                var update = false;
                var updateFacilityAccess = false;
                DssiContact.Contact2 match = null;
                DssiContact.Contact2 updatedContact = new DssiContact.Contact2();

                // check if contact exists
                if (contact.EmployeeId != "" || contact.EmployeeId != "0")
                {
                    foreach (var liveContact in liveContacts.Contacts)
                    {
                        if (liveContact.EmployeeID == contact.EmployeeId)
                        {
                            match = liveContact;
                            break;
                        }
                    }

                    if (match == null)
                    {
                        createRequired = true;
                    }
                }
                else
                {
                    //No EmployeeID in CSV file!
                    Program.WriteLine("***No EmployeeId in Active Directory: " + contact.FirstName + " " + contact.LastName);
                    continue;
                }

                if (createRequired)
                {
                    // create new user
                    DssiContact.Contact2 newContact = new DssiContact.Contact2();
                    newContact.FirstName = contact.FirstName.Length > 15 ? contact.FirstName.Substring(0, 15) : contact.FirstName;
                    newContact.LastName = contact.LastName.Length > 15 ? contact.LastName.Substring(0, 15) : contact.LastName;
                    newContact.EmailAddress = contact.EmailAddress;
                    newContact.UserName = contact.UserName;
                    newContact.Facility = contact.Location1Code;
                    newContact.Job = "UN";
                    newContact.Profile = (uint)contact.ProfileId;

                    if (newContact.Profile == FacilityAdmin)
                    {
                        newContact.UserFlags = new DssiContact.UserFlags();
                        newContact.UserFlags.IsApprover = true;
                    }

                    newContact.Password = Program.GeneratePassword(contact);
                    newContact.EmployeeID = contact.EmployeeId;

                    newContact.Status = new DssiContact.UserStatus();
                    newContact.Status.Active = true;

                    try
                    {
                        if (!Program.test)
                        {
                            contactService.CreateContact(newContact);
                        }
                        Program.WriteLine("Creating new contact: " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ")");
                        Program.WriteLine("Password set to: " + newContact.Password);

                        if (newContact.UserFlags != null && newContact.UserFlags.IsApprover == true)
                        {
                            try
                            {
                                if (!Program.test)
                                {
                                    contactService.AddUserToFacilityApprovalGroupByEmployeeID(newContact.EmployeeID, 1403);
                                }
                                Program.WriteLine("Adding user to approval group: " + newContact.FirstName + " " + newContact.LastName + " (" + newContact.EmployeeID + ")");
                            }
                            catch (Exception e)
                            {
                                Program.WriteLine("***FAILED: Adding user to approval group " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") " + e);
                            }
                        }

                        if (contact.CheckFacilityAccess) {
                            try
                            {
                                if (!Program.test)
                                {
                                    contactService.SetCustomFacilityAccess(contact.EmployeeId, contact.Facilities);
                                }
                                Program.WriteLine("Setting custom facility access for " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ")");
                            }
                            catch (Exception e)
                            {
                                Program.WriteLine("***FAILED: Setting custom facility access for " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") " + e);
                            }
                        }
                        numContactsCreated++;
                    }
                    catch (Exception e)
                    {
                        Program.WriteLine("***FAILED: Creating new contact for " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") " + e);
                    }
                }
                else
                {
                    // check to see if updates necessary

                    if (match.FirstName != contact.FirstName)
                    {
                        update = true;
                        mismatch += match.FirstName + " => " + contact.FirstName + " ";
                        updatedContact.FirstName = contact.FirstName.Length > 15 ? contact.FirstName.Substring(0, 15) : contact.FirstName;
                    }

                    if (match.LastName != contact.LastName)
                    {
                        update = true;
                        mismatch += match.LastName + " => " + contact.LastName + " ";
                        updatedContact.LastName = contact.LastName.Length > 15 ? contact.LastName.Substring(0, 15) : contact.LastName;
                    }

                    if (match.Profile != contact.ProfileId)
                    {
                        update = true;
                        mismatch += match.Profile + " => " + contact.ProfileId + " ";
                        updatedContact.Profile = (uint)contact.ProfileId;
                        contact.UserFlags = new DssiContact.UserFlags();
                        if (contact.ProfileId == FacilityAdmin)
                        {
                            contact.UserFlags.IsApprover = true;
                        }
                        else
                        {
                            contact.UserFlags.IsApprover = false;
                        }
                    }

                    if (contact.CheckFacilityAccess)
                    {
                        var facilityAccess = new DssiContact.FacilityList();
                        try
                        {
                            facilityAccess = contactService.GetCustomFacilityAccess(contact.EmployeeId);
                        }
                        catch (Exception e)
                        {
                            Program.WriteLine("***FAILED: GetCustomFacilityAccess: " + e);
                        }

                        if (facilityAccess != null)
                        {
                            for (int i = 0; i < contact.Facilities.Facilities.Length; i++)
                            {
                                if (facilityAccess.Facilities.Length != contact.Facilities.Facilities.Length)
                                {
                                    Program.WriteLine(facilityAccess.Facilities.Length + " != " + contact.Facilities.Facilities.Length);
                                    foreach (var item in facilityAccess.Facilities)
                                    {
                                        Program.WriteLine(item.FacilityID);
                                    }
                                    updateFacilityAccess = true;
                                    break;
                                }

                                if (facilityAccess.Facilities[i].FacilityID != contact.Facilities.Facilities[i].FacilityID)
                                {
                                    Program.WriteLine(facilityAccess.Facilities[i].FacilityID + " != " + contact.Facilities.Facilities[i].FacilityID);
                                    updateFacilityAccess = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (match.Facility != contact.Location1Code)
                    {
                        update = true;
                        mismatch += match.Facility + " => " + contact.Location1Code + " ";
                        updatedContact.Facility = contact.Location1Code;

                        if (match.Profile == FacilityAdmin)
                        {
                            // Facility change, if they are an FacilityAdmin remove approval for their old facility
                            try
                            {
                                if (!Program.test)
                                {
                                    contactService.RemoveUserFromFacilityApprovalGroupByEmployeeID(match.EmployeeID);
                                }
                                Program.WriteLine("Removing user from old approval group: " + match.FirstName + " " + match.LastName + " (" + contact.EmployeeId + ") ");
                                contact.UserFlags = new DssiContact.UserFlags();
                                contact.UserFlags.IsApprover = true;
                                
                            }
                            catch (Exception e)
                            {
                                Program.WriteLine("***FAILED: Removing user from Facility Approval Group " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") " + e);
                            }
                        }
                    }
                }

                if (update || updateFacilityAccess)
                {
                    try
                    {
                        if (update)
                        {
                            if (!Program.test)
                            {
                                contactService.UpdateInformationByEmployeeID(contact.EmployeeId, updatedContact);
                            }
                            Program.WriteLine("Update: " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") (" + mismatch + ")");
                        }

                        if (contact.UserFlags != null)
                        {
                            try
                            {
                                if (contact.UserFlags.IsApprover == true)
                                {
                                    if (!Program.test)
                                    {
                                        contactService.AddUserToFacilityApprovalGroupByEmployeeID(contact.EmployeeId, 1403);
                                    }
                                    Program.WriteLine("Adding user to approval group: " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") (" + mismatch + ")");
                                }
                                else
                                {
                                    if (!Program.test)
                                    {
                                        contactService.RemoveUserFromFacilityApprovalGroupByEmployeeID(contact.EmployeeId);
                                    }
                                    Program.WriteLine("Removing user from all approval groups: " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") (" + mismatch + ")");
                                }
                            }
                            catch (Exception e)
                            {
                                Program.WriteLine("***FAILED: Modifying user to approval group " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") " + e);
                            }
                        }

                        if (updateFacilityAccess)
                        {
                            try
                            {
                                if (!Program.test)
                                {
                                    contactService.RemoveUserFromFacilityApprovalGroupByEmployeeID(contact.EmployeeId);
                                    contactService.SetCustomFacilityAccess(contact.EmployeeId, contact.Facilities);
                                }
                                Program.WriteLine("Updating Facility Access for " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") to:");
                                foreach (var item in contact.Facilities.Facilities)
                                {
                                    Program.WriteLine(item.FacilityID);
                                }
                            }
                            catch (Exception e)
                            {
                                Program.WriteLine("***FAILED: Updating Facility Access for " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") " + e);
                            }
                        }
                        numContactsUpdated++;
                    }
                    catch (Exception e)
                    {
                        Program.WriteLine("***FAILED: Update " + contact.FirstName + " " + contact.LastName + " (" + contact.EmployeeId + ") " + e);
                    }
                }
            }

            // now check all active contacts in DSSI
            var numContactsDeleted = 0;
            foreach (var liveContact in liveContacts.Contacts)
            {
                //Skip over any internal DSSI accounts
                if(liveContact.UserName.StartsWith("6767") || liveContact.EmailAddress.ToLower().IndexOf("dssi.net") != -1 || liveContact.EmailAddress.ToLower().IndexOf("directsupply.com") != -1 || liveContact.EmailAddress.ToLower().IndexOf("directs.com") != -1){
                    //Program.WriteLine(liveContact.FirstName + " " + liveContact.LastName + " " + liveContact.EmailAddress + " excluded because internal email address");
                    continue; 
                }

                // foreach DSSI contact, verify they are still valid
                Contact match = null;
                if (liveContact.MasterUserId != 0)
                {
                    //There are some old accounts that need to be deactivated--this code can likely be removed after a full initial run
                    if (liveContact.EmployeeID == "")
                    {
                        //Program.WriteLine("NO EMPLOYEEID Marking contact Inactive: " + liveContact.FirstName + " " + liveContact.LastName + " -- " + liveContact.Job + " (MUID: " + liveContact.MasterUserId + " -- " + liveContact.LastLoginDate + " DSSI EmployeeID: " + liveContact.EmployeeID + ")");
                        match = null;
                    }
                    else
                    {                                                 
                        match = contacts.Find(x => x.EmployeeId == liveContact.EmployeeID);
                        if(match == null)
                        {
                            if (liveContact.EmployeeID == "124954")
                            {
                                Program.WriteLine("No match found: " + liveContact.FirstName + " " + liveContact.LastName + " -- " + liveContact.Job + " (MUID: " + liveContact.MasterUserId + " -- " + liveContact.LastLoginDate + " DSSI EmployeeID: " + liveContact.EmployeeID + ")");
                            }
                        }
                    }

                    if (match == null)
                    {
                        //Skip over any excluded accounts
                        Exclusion excluded = exclusions.Find(x => x.MasterUserId == liveContact.MasterUserId);
                        if(excluded != null){
                            continue;
                        }

                        // mark user as inactive
                        Program.WriteLine("Marking contact Inactive: " + liveContact.FirstName + " " + liveContact.LastName + " -- " + liveContact.Job + " (MUID: " + liveContact.MasterUserId + " -- " + liveContact.LastLoginDate+" DSSI EmployeeID: "+liveContact.EmployeeID+")");                           
                        DssiContact.Contact2 inactiveContact = new DssiContact.Contact2();

                        inactiveContact.Status = new DssiContact.UserStatus();
                        inactiveContact.Status.Active = false;

                        try
                        {
                            if (!Program.test)
                            {
                                contactService.UpdateInformationByMasterUserID((int)liveContact.MasterUserId, inactiveContact);
                            }
                            numContactsDeleted++;
                        }
                        catch (Exception e)
                        {
                            Program.WriteLine("Error Marking Contact Inactive: " + e);
                        }
                    }
                }
            }

            // report results
            DssiContact.ContactList2 updatedContacts = new DssiContact.ContactList2();
            DssiContact.ApprovalGroupList updApprovalGroups = new DssiContact.ApprovalGroupList();
            var facility = "";

            updatedContacts = contactService.GetAllActiveContacts();
            var csv = new StringBuilder();
            csv.AppendLine("First Name,Last Name,Profile,Employee ID,Job Code,Facility Code,Facility Name, Facilities with Approval Access");
            foreach (var updatedContact in updatedContacts.Contacts)
            {
                var locName = "Not Found";
                try
                {
                    Location loc = locations.Find(x => x.LocationCode == updatedContact.Facility);
                    locName = loc.LocationName;

                    updApprovalGroups = contactService.GetAllApprovalGroupsByEmployeeID(updatedContact.EmployeeID);
                    if (updApprovalGroups.ApprovalGroups[0] != null)
                    {
                        facility = updApprovalGroups.ApprovalGroups[0].FacilityID;
                    }
                    else
                    {
                        facility = "";
                    }
                }
                catch(Exception e)
                {
                    Program.WriteLine("Could not find location specified in DSSI for user: " + updatedContact.FirstName + " " + updatedContact.LastName);
                }

                var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", updatedContact.FirstName, updatedContact.LastName, updatedContact.Profile, updatedContact.EmployeeID, updatedContact.Job, updatedContact.Facility, locName, facility);
                csv.AppendLine(newLine);

                var i = 0;
                foreach (DssiContact.ApprovalGroup currentApproval in updApprovalGroups.ApprovalGroups)
                {
                    if (i == 0) { continue; }
                    var approvalGroupLine = string.Format(",,,,,,{0}", updApprovalGroups.ApprovalGroups[i].FacilityID);
                    csv.AppendLine(newLine);
                    i++;

                }
            }

            string outputCsvFile = config.AppPath + "\\DssiUsers_" + DateTime.Now.ToString("MM-dd-yyyy-HHmm") + ".csv";
            Program.WriteLine("Creating output contact file: " + outputCsvFile);
            File.WriteAllText(outputCsvFile, csv.ToString());

            Program.WriteLine(numContactsCreated + " Contacts Created");
            Program.WriteLine(numContactsUpdated + " Contacts Updated");
            Program.WriteLine(numContactsDeleted + " Contacts Deactivated");
            Program.WriteLog();
            Program.Debug();
        }

        static string GeneratePassword(Contact contact)
        {
            var chars = contact.UserName;
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            return contact.EmailAddress + finalString;
       
        }

        private static void Debug()
        {
            if (Program.debug)
            {
                Console.WriteLine("SRG Contact Updater Completed Successfully. Press enter to exit.");
                if (System.Diagnostics.Debugger.IsAttached) Console.ReadLine();
                return;
            }

        }

        static void WriteLine(string text)
        {
            Console.WriteLine(text);
            Program.log = Program.log + text + Environment.NewLine;

        }

        static void WriteLog()
        {
            var file = config.AppPath + "\\log.txt";
            Program.WriteLine("Writing log to file: " + file);
            System.IO.File.WriteAllText(file, Program.log);
        }
    } 
}
