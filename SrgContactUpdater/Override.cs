using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrgContactUpdater
{
    class Override
    {
        public String Name { get; set; }
        public String EmployeeId { get; set; }
        public Int32 ProfileId { get; set; }
        public String Location1Id { get; set; }
        public String Location2Id { get; set; }
        public String Location3Id { get; set; }
        public String Location4Id { get; set; }
        public String Location5Id { get; set; }
        public String Location6Id { get; set; }
        public String Location7Id { get; set; }
        public String Location8Id { get; set; }
        public String Location9Id { get; set; }
    }
}
